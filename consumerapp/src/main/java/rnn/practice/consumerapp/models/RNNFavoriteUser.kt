package rnn.practice.consumerapp.models

data class RNNFavoriteUser(
    var loginString: String? = null,
    var avatarString: String? = null
)
