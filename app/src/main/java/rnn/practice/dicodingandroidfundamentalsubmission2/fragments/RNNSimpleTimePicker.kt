package rnn.practice.dicodingandroidfundamentalsubmission2.fragments

import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Context
import android.os.Bundle
import android.widget.TimePicker
import androidx.fragment.app.DialogFragment
import java.util.*

class RNNSimpleTimePicker : DialogFragment(), TimePickerDialog.OnTimeSetListener {

    private var currentDialogTimeEventListener: DialogTimeEventListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        currentDialogTimeEventListener = context as DialogTimeEventListener?
    }

    override fun onDetach() {
        super.onDetach()
        currentDialogTimeEventListener = null
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return TimePickerDialog(activity, this, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE), true)
    }

    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        currentDialogTimeEventListener?.onTimeSet(tag, hourOfDay, minute)
    }

    interface DialogTimeEventListener {
        fun onTimeSet(tag: String?, hourOfDay: Int, minute: Int)
    }
}