package rnn.practice.dicodingandroidfundamentalsubmission2.rnn_search

enum class RNNSearchState {
    INIT, LOAD, FINISH, FAIL
}