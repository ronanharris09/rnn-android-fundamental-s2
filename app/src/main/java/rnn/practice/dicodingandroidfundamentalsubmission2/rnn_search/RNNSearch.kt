package rnn.practice.dicodingandroidfundamentalsubmission2.rnn_search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.core.Response
import com.github.kittinunf.fuel.coroutines.awaitStringResponseResult
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import kotlinx.coroutines.*
import rnn.practice.dicodingandroidfundamentalsubmission2.BuildConfig
import rnn.practice.dicodingandroidfundamentalsubmission2.models.RNNGithubUserDetails
import rnn.practice.dicodingandroidfundamentalsubmission2.models.RNNGithubUserMain
import rnn.practice.dicodingandroidfundamentalsubmission2.models.RNNGithubUserSearch

class RNNSearch(private val uiState: SavedStateHandle) : ViewModel() {

    // Local vars and objects to store stuff
    private val headerProps: Map<String, String> = mapOf("Authorization" to BuildConfig.RNN_GITHUB_TOKEN, "Accept" to "application/json", "User-Agent" to "RNN Dicoding Android Fundamental Submission 2 Practice [Testing]")
    private val currentUsers = MutableLiveData<RNNGithubUserSearch>()
    private val currentUser = MutableLiveData<RNNGithubUserDetails>()
    private val currentFollower = MutableLiveData<ArrayList<RNNGithubUserMain>>()
    private val currentFollowing = MutableLiveData<ArrayList<RNNGithubUserMain>>()
    var currentState = RNNSearchState.INIT
    val liveDetailsActivityCreate = uiState.getLiveData("detailsActivityLock", true)
    val liveMainActivityComplete = uiState.getLiveData("mainActivityLock", true)

    fun setLiveDetailsActivityCreate(current: Boolean) {
        uiState.set("detailsActivityLock", current)
    }

    fun setLiveMainActivityComplete(current: Boolean) {
        uiState.set("mainActivityLock", current)
    }

    fun getUsers() : LiveData<RNNGithubUserSearch> = currentUsers
    fun getUser() : LiveData<RNNGithubUserDetails> = currentUser
    fun getFollowers() : LiveData<ArrayList<RNNGithubUserMain>> = currentFollower
    fun getFollowing() : LiveData<ArrayList<RNNGithubUserMain>> = currentFollowing

    suspend fun queryUsers(queryString: String, stateListener: (RNNSearchState) -> Unit) = queryGithub("https://api.github.com/search/users?q=$queryString&per_page=100", 0, false, stateListener)
    suspend fun queryUser(loginString: String, stateListener: (RNNSearchState) -> Unit) = queryGithub("https://api.github.com/users/$loginString", 1,  false, stateListener)
    suspend fun queryFollowers(queryString: String, stateListener: (RNNSearchState) -> Unit) = queryGithub("https://api.github.com/users/$queryString/followers?per_page=100", 0,  true, stateListener)
    suspend fun queryFollowing(queryString: String, stateListener: (RNNSearchState) -> Unit) = queryGithub("https://api.github.com/users/$queryString/following?per_page=100", 1,  true, stateListener)


    private suspend fun queryGithub(queryPath: String,currentMode: Int, isArray: Boolean,stateListener: (RNNSearchState) -> Unit) {
        stateListener(updateCurrentState(RNNSearchState.LOAD))

        fuelQuery(queryPath).apply {
            this.third.fold(
                {
                    if (isArray) {
                        with(ArrayList<RNNGithubUserMain>()) {
                            this.addAll(Gson().fromJson(it, Array<RNNGithubUserMain>::class.java))
                            when (currentMode) {
                                0 -> {
                                    currentFollower.postValue(this)
                                }
                                1 -> {
                                    currentFollowing.postValue(this)
                                }
                            }
                        }
                    } else {
                        when (currentMode) {
                            0 -> {
                                currentUsers.postValue(Gson().fromJson(it, RNNGithubUserSearch::class.java))
                            }
                            1 -> {
                                currentUser.postValue(Gson().fromJson(it, RNNGithubUserDetails::class.java))
                            }
                        }
                    }

                    stateListener(updateCurrentState(RNNSearchState.FINISH))
                },
                {
                    stateListener(updateCurrentState(RNNSearchState.FAIL))
                }
            )
        }
    }

    private suspend fun fuelQuery(pathString: String): Triple<Request, Response, Result<String, FuelError>> = Fuel.get(pathString).header(headerProps).awaitStringResponseResult()

    private fun updateCurrentState(newState: RNNSearchState) : RNNSearchState {
        currentState = newState

        return currentState
    }
}