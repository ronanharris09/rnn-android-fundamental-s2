package rnn.practice.dicodingandroidfundamentalsubmission2.receivers

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.util.Log
import androidx.core.app.NotificationCompat
import rnn.practice.dicodingandroidfundamentalsubmission2.MainActivity
import rnn.practice.dicodingandroidfundamentalsubmission2.R
import java.util.*

class RNNAlarmReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        Log.d("RNNALARM", "STATE onReceive()")

        var alarmTitle: String
        var alarmMessage: String

        intent!!.apply {
            alarmTitle = getStringExtra("RNN_ALARM_TITLE")!!
            alarmMessage = getStringExtra("RNN_ALARM_MESSAGE")!!
        }

        (context?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).let {
            it.createNotificationChannel(NotificationChannel("RNNChannel_180791", "RNNAlarmChannel AFS2", NotificationManager.IMPORTANCE_DEFAULT))
            it.notify(
                187,
                NotificationCompat.Builder(context, "RNNChannel_180791").apply {
                    setSmallIcon(R.drawable.ic_baseline_alarm_24)
                    setContentTitle(alarmTitle)
                    setContentText(alarmMessage)
                    setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    setChannelId("RNNChannel_180791")
                    setContentIntent(PendingIntent.getActivity(context, 0, Intent(context, MainActivity::class.java), PendingIntent.FLAG_UPDATE_CURRENT))
                }.build()
            )
        }
    }
}