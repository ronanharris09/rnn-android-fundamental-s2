package rnn.practice.dicodingandroidfundamentalsubmission2.interfaces

import androidx.room.*
import kotlinx.coroutines.flow.Flow
import rnn.practice.dicodingandroidfundamentalsubmission2.entity.RNNGithubUserFavorite

@Dao
interface RNNGithubUserFavoriteDAO {
    // RETURN all RNNGithubUserFavorite
    @Query("SELECT * FROM table_favorite")
    fun getFavorites(): Flow<List<RNNGithubUserFavorite>>

    // INSERT a RNNGithubUserFavorite
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addFavorite(currentRNNGithubUserFavorite: RNNGithubUserFavorite)

    // RETURN a RNNGithubUserFavorite
    @Query("SELECT * FROM table_favorite WHERE _id = :currentLoginString")
    suspend fun findFavorite(currentLoginString: String) : RNNGithubUserFavorite?

    // REMOVE a RNNGithubUserFavorite
    @Delete
    suspend fun removeFavorite(currentRNNGithubUserFavorite: RNNGithubUserFavorite)

    // RESET table_favorite
    @Query("DELETE FROM table_favorite")
    suspend fun resetFavorite()
}