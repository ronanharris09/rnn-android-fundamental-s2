package rnn.practice.dicodingandroidfundamentalsubmission2.models

data class RNNUserPreferences(
    var isActive: Boolean = false,
    var isCustomTime: Boolean = false,
    var currentReminderTime: String? = null
)
