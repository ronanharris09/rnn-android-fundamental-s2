package rnn.practice.dicodingandroidfundamentalsubmission2.repository

import androidx.annotation.WorkerThread
import androidx.lifecycle.asLiveData
import rnn.practice.dicodingandroidfundamentalsubmission2.entity.RNNGithubUserFavorite
import rnn.practice.dicodingandroidfundamentalsubmission2.interfaces.RNNGithubUserFavoriteDAO

class RNNGithubUserFavoriteRepository(private val currentRNNGithubUserFavoriteDAO: RNNGithubUserFavoriteDAO) {

    val listFavorites = currentRNNGithubUserFavoriteDAO.getFavorites().asLiveData()

    @WorkerThread
    suspend fun add(currentRNNGithubUserFavorite: RNNGithubUserFavorite) {
        currentRNNGithubUserFavoriteDAO.addFavorite(currentRNNGithubUserFavorite)
    }

    @WorkerThread
    suspend fun find(currentLoginString: String) : RNNGithubUserFavorite? {
        return currentRNNGithubUserFavoriteDAO.findFavorite(currentLoginString)
    }

    @WorkerThread
    suspend fun delete(currentRNNGithubUserFavorite: RNNGithubUserFavorite) {
        currentRNNGithubUserFavoriteDAO.removeFavorite(currentRNNGithubUserFavorite)
    }

    @WorkerThread
    suspend fun reset() {
        currentRNNGithubUserFavoriteDAO.resetFavorite()
    }
}